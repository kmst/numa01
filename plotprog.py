#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  plotprog.py
#
#  Copyright 2015-2016 Karl Lindén <karl.linden.887@student.lu.se>
#  Copyright 2015-2016 Markus Oskarsson <nat15mos@student.lu.se>
#  Copyright 2015-2016 Sölve Ryding <solve.ryding.254@student.lu.se>
#  Copyright 2015-2016 Ted Skyvell <Ted.skyvell.026@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

# Plotting program implentation.

import argparse
import matplotlib.pyplot as plt
from fractal import Fractal2D

def plotprog(function, plot_type = None, **kwargs):
    """
    Implementation of a plotting program. All arguments except function can be
    overridden from the command line.

    Arguments:
        function  -- the function to plot
        plot_type -- default plot type
        **kwargs  -- keyword arguments that should override the defaults set by
                     the class (these given defaults can the be overridden by
                     from command line)
    """

    # If plot_type is not set to some plot type, then just take some random
    # default.
    if not plot_type:
        plot_type = 'idx'
    plot_types = [
        ('idx'  ,  'plots the index of the zero that each point converges to'),
        ('iter2d', '2D-plot the number of iterations the algorithm takes to '
                   'converge for each point.'),
        ('iter3d', '3D-plot the number of iterations the algorithm takes to '
                   'converge for each point')
    ]

    parser = argparse.ArgumentParser(description='Plot something')

    # Export each plot type to the command line.
    for pt,help in plot_types:
        parser.add_argument('--' + pt, dest='plot_type', action='store_const',
                            const=pt, default=plot_type, help=help)

    parser.add_argument('--numjac', action='store_const', const=True,
                        default=False, help='compute jacobian numerically')

    bool_options = [
        ('simplified', 'run the simplified method'),
        ('zeroes'    , 'plot zeroes in index plot'),
    ]

    for opt,help in bool_options:
        parser.add_argument('--' + opt, action='store_const', const=True,
                            default=False, help=help)

    # Export important options to the command line to make them user settable.
    options = [
        ('xmin'   , float, 'minimal x-value'                    ),
        ('xmax'   , float, 'maximal x-value'                    ),
        ('ymin'   , float, 'minimal y-value'                    ),
        ('ymax'   , float, 'maximal y-value'                    ),
        ('N'      , int,   'number of subdivision for each axis'),
        ('maxiter', int,   'maximum number of iterations'       ),
        ('jacprec', float, 'jacobian precision'                 ),
        ('width'  , int,   'width (in inches)'                  ),
        ('height' , int,   'height (in inches)'                 ),
        ('xlabel' , str,   'x label'                            ),
        ('ylabel' , str,   'y label'                            ),
        ('zlabel' , str,   'z label'                            ),
        ('cmap'   , str,   'color map to use'                   ),
    ]
    for opt,argtype,help in options:
        hyph = '-'
        if len(opt) > 1:
            hyph += '-'
        parser.add_argument(hyph + opt, type=argtype, help=help)

    parser.add_argument('--output', '-o', type=str, help='output file')

    args = parser.parse_args()

    # Import boolean options from the command line.
    for opt,help in bool_options:
        if getattr(args, opt):
            kwargs[opt] = True

    # Import any given options from the command line and replace their values
    # in kwargs so that they are passed down onto the sub-routines.
    for opt,argtype,help in options:
        if hasattr(args, opt):
            val = getattr(args, opt)
            if val:
                kwargs[opt] = val

    # Remove derivative if the numerical jacobian should be used.
    if args.numjac:
        del kwargs['derivative']

    fractal = Fractal2D(function, **kwargs)

    # Plot the desired plot.
    plot_type = args.plot_type
    plot_func = getattr(fractal, plot_type + '_plot')
    plot_func(**kwargs)

    # If an output was given on the command line, output to it now.
    if args.output:
        plt.savefig(args.output)
