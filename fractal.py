#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  fractal.py
#
#  Copyright 2015-2016 Karl Lindén <karl.linden.887@student.lu.se>
#  Copyright 2015-2016 Markus Oskarsson <nat15mos@student.lu.se>
#  Copyright 2015-2016 Sölve Ryding <solve.ryding.254@student.lu.se>
#  Copyright 2015-2016 Ted Skyvell <Ted.skyvell.026@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d

class Fractal2D(object):
    def __init__(self, function, derivative = None, jacprec = 1e-3, **kwargs):
        """
        Initializes an instance of Fractal2D.

        Arguments:
            function -- the function to plot
            derivate -- the derivative of the function
            jacprec  -- precision of the numeric jacobian (h-value)
            **kwargs -- keyword arguments (ignored)
        """
        self.zeroes = []
        self.function = function
        self.h = jacprec
        if derivative:
            self.derivative = derivative
        else:
            self.derivative = self.jacobian

    def newton(self, guess, simplified = False, tol = 1e-7, maxiter = 30,
               **kwargs):
        """
        Runs Newton's algorithm on the given guess.

        Arguments:
            guess      -- initial points, size (M, N, 2)
            simplified -- whether or not to use the simplified algorithm
            tol        -- absolute zero tolerance
            maxiter    -- maximal number of iterations

        Returns:
            an array of zeroes for each point in guess, size (M, N, 2), with
            the points that did not converge masked
        """
        x = guess
        y = self.function(x)

        if simplified:
            single = self.jacobian(x)
            derivative = lambda x: single
        else:
            derivative = self.derivative

        A = np.zeros_like(x[..., 0])
        for i in range(maxiter):
            """ The new x is solved from the folowing equation:
            0 = f'(x_0)(x - x_0) + f(x_0)   # Think 0 = k(x - a) + m
            -f(x_0) = f'(x_0)(x - x_0)
            x - x_0 = f'(x_0)^(-1) * -f(x_0)
            x = x_0 - f'(x_0)^(-1) * f(x_0)
            x = x_0 - linalg.solve(f'(x_0), f(x_0))

            The ufunc is used because linalg.solve error behavior is a pain.
            """
            x = x - np.linalg._umath_linalg.solve1(derivative(x), y)
            y = self.function(x)

            m = np.prod(np.isclose(y, 0, rtol = 0, atol = tol), axis = -1)
            mn = np.logical_not(m)

            # A is a per XY pair iteration counter
            A += mn
            if np.allclose(y, 0, rtol = 0, atol = tol):
                break
        else:
            A = np.ma.array(A, mask = mn)

        return np.ma.array(x, mask = np.stack([mn, mn], axis = -1)), A

    def zero(self, guess, ztol = 5, **kwargs):
        """
        Determines the indices of the zeroes that newton's method converges to.

        Arguments:
            guess -- an array of points, size (M, N, 2)
            ztol  -- tolerance for a zero

        Returns:
            the index of zeroes for the guess, size (M, N)
        """
        x = self.newton(guess, **kwargs)[0]
        x = x[..., 0] + x[..., 1] * 1j
        self.zeroes, a = np.unique(np.around(x, ztol), return_inverse = True)
        return np.reshape(a, x.shape)

    def jacobian(self, x):
        """
        Compute the jacobian of the function at an array of points.

        Arguments:
            x -- an array of points of size (M, N, 2)

        Returns:
            the jacobian of the points as an array of size (M, N, 2, 2)
        """
        h = self.h
        col1 = (self.function(x + [[h, 0]]) - self.function(x)) / h
        col2 = (self.function(x + [[0, h]]) - self.function(x)) / h
        return np.stack([col1, col2], axis = 3)

    def _axes(self, xmin = -1, xmax = 1, ymin = -1, ymax = 1, N = 100,
              **kwargs):
        """
        Internal helper function that creates the axes _and_ states the default
        window values.

        Arguments:
            xmin -- minimum x-value [-1]
            xmax -- maximum x-value [ 1]
            ymin -- minimum y-value [-1]
            ymax -- maximum y-value [ 1]
            N    -- number of subdivisions for each axis [100]

        Returns:
            A tuple of (X, Y, XY).
        """
        X = np.linspace(xmin, xmax, N)
        Y = np.linspace(ymin, ymax, N)
        XY = np.rollaxis(np.array(np.meshgrid(X, Y)), 0, 3)
        return X, Y, XY

    def _figure(self, plot3d = False,
                width = 10, height = 8,
                xlabel = 'x', ylabel = 'y', zlabel = 'iterations',
                **kwargs):
        """
        Internal helper function that sets the figure size and sets axis labels.

        Arguments:
            plot3d -- whether or not to 3D-plot [False]
            width  -- width of the figure (in inches) [8]
            height -- height of the figure (in inches) [8]
            xlabel -- desired x-axis label ['x']
            ylabel -- desired y-axis label ['x']
            zlabel -- desired z-axis label ['iterations']

        Returns:
            The current Axes instance.
        """
        projection = None
        if plot3d:
            projection = '3d'
        fig = plt.figure(figsize = (width, height))
        ax = fig.gca(projection = projection)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        if plot3d:
            ax.set_zlabel(zlabel)
        return ax

    def _cmap(self, default, **kwargs):
        """
        Select a colour map by respecting the 'cmap' key in kwargs if it exists
        and falling back to a specified default if not.

        Arguments:
            default -- the default colour map to use

        Returns:
            a colour map
        """
        if 'cmap' in kwargs:
            try:
                return getattr(plt.cm, kwargs['cmap'])
            except:
                print('error: invalid color map, using default')
        return default

    def _discrete_norm(self, cmap):
        """
        Computes a norm that discretisizes the colour map.

        Arguments:
            cmap -- the colour map to use

        Returns:
            A norm computed from the length of self.zeroes.
        """

        # Define bins and normalize.
        if len(self.zeroes) == 0:
            raise BaseException('you must call self.zero before this function')
        bounds = np.linspace(0, len(self.zeroes), len(self.zeroes) + 1)
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

        return norm

    def idx_plot(self, zeroes=False, **kwargs):
        """Plots the index of the zero that each point converges to.

        Arguments:
            **kwargs -- arguments passed down to sub-routines, see for example
                        _axes() and _figure()
        """
        self._figure(**kwargs)
        X, Y, XY = self._axes(**kwargs)

        data = self.zero(XY, **kwargs)

        cmap = self._cmap(plt.cm.viridis, **kwargs)
        norm = self._discrete_norm(cmap)
        plt.pcolor(X, Y, data, cmap = cmap, norm = norm)
        if zeroes:
            plt.plot(np.real(self.zeroes), np.imag(self.zeroes), 'wo')
        plt.colorbar()

    def iter2d_plot(self, **kwargs):
        """2D plots the number of iterations it takes for newton's algorithm to
        find a zero for each point.

        Arguments:
            **kwargs -- arguments passed down to sub-routines, see for example
                        _axes() and _figure()
        """
        self._figure(**kwargs)
        X, Y, XY = self._axes(**kwargs)

        cmap = self._cmap(plt.cm.hsv, **kwargs)
        plt.pcolor(X, Y, self.newton(XY, **kwargs)[1], cmap = cmap)
        plt.colorbar()

    def iter3d_plot(self, **kwargs):
        """3D plots the number of iterations it takes for newton's algorithm to
        find a zero for each point.

        Arguments:
            **kwargs -- arguments passed down to sub-routines, see for example
                        _axes() and _figure()
        """

        # Modify default width and height.
        if not 'width' in kwargs:
            kwargs['width'] = 10
        if not 'height' in kwargs:
            kwargs['height'] = 6

        X, Y, XY = self._axes(**kwargs)
        ax = self._figure(plot3d = True, **kwargs)
        ax.plot_surface(XY[..., 0], XY[..., 1], self.newton(XY, **kwargs)[1],
                        cmap = plt.cm.ocean, rstride = 1, cstride = 1,
                        linewidth = 0)
