#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  f2.py
#
#  Copyright 2015-2016 Karl Lindén <karl.linden.887@student.lu.se>
#  Copyright 2015-2016 Markus Oskarsson <nat15mos@student.lu.se>
#  Copyright 2015-2016 Sölve Ryding <solve.ryding.254@student.lu.se>
#  Copyright 2015-2016 Ted Skyvell <Ted.skyvell.026@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import numpy as np
from plotprog import plotprog

def F_2(x):
    x_1 = np.array(x)[..., 0]
    x_2 = np.array(x)[..., 1]
    y_1 = (     x_1**8          -
           28 * x_1**6 * x_2**2 +
           70 * x_1**4 * x_2**4 +
           15 * x_1**4          -
           28 * x_1**2 * x_2**6 -
           90 * x_1**2 * x_2**2 +
                         x_2**8 +
           15 *          x_2**4 -
           16)
    y_2 = ( 8 * x_1**7 * x_2    -
           56 * x_1**5 * x_2**3 +
           56 * x_1**3 * x_2**5 +
           60 * x_1**3 * x_2    -
           8  * x_1    * x_2**7 -
           60 * x_1    * x_2**3)
    return np.stack([y_1, y_2], axis = -1)

def f_2(x):
    x_1 = np.array(x)[..., 0]
    x_2 = np.array(x)[..., 1]
    y_11 = 4 * x_1 * (  2 * x_1**6          -
                       42 * x_1**4 * x_2**2 +
                        5 * x_1**2 * (14 * x_2**4 + 3 ) -
                            x_2**2 * (14 * x_2**4 + 45))
    y_12 = 4 * x_2 * (-14 * x_1**6          +
                       70 * x_1**4 * x_2**2 -
                        3 * x_1**2 * (14 * x_2**4 + 15) +
                            x_2**2 * ( 2 * x_2**4 + 15))
    y_21 = 4 * x_2 * ( 14 * x_1**6          -
                       70 * x_1**4 * x_2**2 +
                            x_1**2 * (42 * x_2**4 + 45) -
                            x_2**2 * ( 2 * x_2**4 + 15))
    y_22 = 4 * x_1 * (  2 * x_1**6          -
                       42 * x_1**4 * x_2**2 +
                        5 * x_1**2 * (14 * x_2**4 + 3 ) -
                            x_2**2 * (14 * x_2**4 + 45))
    y_1 = np.stack([y_11, y_12], axis = -1)
    y_2 = np.stack([y_21, y_22], axis = -1)
    return np.stack([y_1, y_2], axis = -2)

plotprog(F_2, derivative = f_2, maxiter = 100)
